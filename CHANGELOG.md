## [1.10.3] - 2023-12-20
### Fixed
- Fatal error: Uncaught Error: Call to undefined function IC\Theme\Core\wpcf7_enqueue_scripts()

## [1.10.2] - 2023-04-29
### Added
- Modify jquery position

## [1.10.1] - 2023-02-15
### Fixed
- Visibility of language switcher for single language

## [1.10.0] - 2023-02-15
### Removed
- Embed disabled
### Changed
- Language switcher

## [1.9.2] - 2023-01-13
### Fixed
-  jquery in footer and min version

## [1.9.1] - 2023-01-08
### Fixed
- JSON parse

## [1.9.0] - 2023-01-04
### Added
- Language switcher (widget)

## [1.8.0] - 2023-01-02
### Added
- preloading functionality

- ## [1.7.0] - 2023-01-02
### Added
- ic_get_attachment_image() function

## [1.6.1] - 2022-12-20
### Changed
- Lazy Loading

## [1.6.0] - 2022-12-18
### Added
- Support async scripts

## [1.5.3] - 2022-12-07
### Removed
- Google Maps API key filter

## [1.5.2] - 2022-11-05
### Added
- Support for disable / enable widget editor

## [1.5.1] - 2022-10-11
### Added
- Support for enable / disable embedding

## [1.5.0] - 2022-09-26
### Added
- Support for Gutenberg

## [1.4.0] - 2022-09-01
### Changed
- WooCommerce Integration

## [1.3.4] - 2022-07-08
### Changed
- Versioning on demand

## [1.3.3] - 2022-06-30
### Changed
- Rewrite rules system

## [1.3.2] - 2022-06-23
### Fixed
- WooCommerce Integration (Disable CSS)

## [1.3.1] - 2022-06-23
### Fixed
- WooCommerce Integration (Disable CSS)

## [1.3.0] - 2022-06-23
### Added
- WooCommerce Integration (Disable CSS)

## [1.2.3] - 2022-06-23
### Fixed
- scripts for PDP

## [1.2.2] - 2022-06-14
### Fixed
- versioning assets

## [1.2.1] - 2022-05-27
### Added
- support for templates

## [1.2.0] - 2022-05-17
### Added
- translations
- sections support
### Changed
- flexible content functionality

## [1.1.0] - 2022-02-09
### Added
- Logo widget

## [1.0.8] - 2022-01-30
### Added
- Default primary menu

## [1.0.7] - 2022-01-30
### Fixing
- assets location

## [1.0.6] - 2022-01-23
### Fixing
- namespaces and structure

## [1.0.5] - 2022-01-22
### Fixing
- namespaces and structure

## [1.0.4] - 2022-01-22
### Fixing
- namespaces and structure

## [1.0.3] - 2022-01-22
### Fixing
- namespaces and structure

## [1.0.2] - 2022-01-22
### Fixing
- namespaces and structure

## [1.0.1] - 2022-01-22
### Fixing
- namespaces

## [1.0.0] - 2022-01-22
### Added
- initial version
