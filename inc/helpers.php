<?php

/**
 * Check AJAX request.
 *
 * @return bool
 */
function ic_is_ajax_request(): bool {
	return strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ?? '' ) === 'xmlhttprequest';
}

/**
 * @return int
 */
function ic_get_current_page_num(): int {
	return (int) max( [ get_query_var( 'paged', 1 ), 1 ] );
}

/**
 * Get an HTML img element representing an image attachment
 * While `$size` will accept an array, it is better to register a size with
 * add_image_size() so that a cropped version is generated. It's much more
 * efficient than having to find the closest-sized image and then having the
 * browser scale down the image.
 *
 * @param int          $attachment_id Image attachment ID.
 * @param string|array $size          Optional. Image size. Accepts any valid image size, or an array of width
 *                                    and height values in pixels (in that order). Default 'thumbnail'.
 * @param bool         $icon          Optional. Whether the image should be treated as an icon. Default false.
 * @param string|array $attr          Optional. Attributes for the image markup. Default empty.
 *
 * @return string HTML img element or empty string on failure.
 */
function ic_get_attachment_image( int $attachment_id, $size = 'thumbnail', bool $icon = false, $attr = '' ): string {
	if ( get_post_mime_type( $attachment_id ) === 'image/svg+xml' ) {
		$file = get_attached_file( $attachment_id );

		if ( file_exists( $file ) ) {
			return file_get_contents( $file );
		}
	}

	return wp_get_attachment_image( $attachment_id, $size, $icon, $attr );
}
