<?php
/**
 * Template Name: Flexible Content
 * Template Post Type: page
 */

use IC\Functionality\ACF\FlexibleContent\FlexibleContent;

the_post();
get_header(); ?>

	<section class="page-content">
		<?php ( new FlexibleContent() )->render(); ?>
	</section>

<?php
get_footer();
