<?php
/**
 * @var array $args      Widget arguments.
 * @var array $instance  Saved values from database.
 * @var array $languages .
 */

defined( 'ABSPATH' ) || exit;

?>

<ul class="menu">
	<?php foreach ( $languages as $language ) : ?>
		<li class="menu-item
		<?php
		if ( $language['active'] ) :
			?>
			active<?php endif; ?>">
			<a href="<?php echo esc_url( $language['url'] ); ?>">
				<?php echo wp_kses_post( mb_strtoupper( $language['code'] ) ); ?>
			</a>
		</li>
	<?php endforeach; ?>
</ul>
