<?php
/**
 * @var array $args     Widget arguments.
 * @var array $instance Saved values from database.
 */

defined( 'ABSPATH' ) || exit;

if ( has_custom_logo() ) {
	the_custom_logo();
}
