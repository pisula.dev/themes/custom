<?php
/**
 * Basic Theme
 *
 * @package Theme
 */

namespace IC\Theme\Core;

use IC\Theme\Integration\WooCommerce\WooCommerce;
use IC\Theme\Module\CustomClass;
use IC\Theme\Module\DisableEmoji;
use IC\Theme\Module\LazyLoading;
use IC\Theme\Module\RewriteRules;
use IC\Theme\Module\ThumbnailDemand;
use IC\Theme\Preload;
use IC\Theme\Widget\LanguageSwitcher;
use IC\Theme\Widget\Logo;
use WP_Admin_Bar;
use WP_Screen;

/**
 * Class Theme_Base
 *
 * @package Theme\Core
 */
abstract class Base {

	/**
	 * Which post type support block editor aka Gutenberg
	 *
	 * @var string[]
	 */
	protected array $post_types_supported_gutenberg = [];

	/**
	 * @var bool
	 */
	protected bool $use_widget_block_editor = false;

	/**
	 * @var WooCommerce
	 */
	protected WooCommerce $woocommerce_integration;

	/**
	 * @var string[]
	 */
	protected array $async_scripts = [ 'main' ];

	/** @var Preload */
	protected Preload $preload;

	/** @var bool */
	protected bool $jquery_in_footer = true;

	/**
	 * Theme_Base constructor.
	 */
	public function __construct() {
		if ( apply_filters( 'theme/module/disable_emoji', true ) ) {
			new DisableEmoji();
		}

		if ( apply_filters( 'theme/module/lazy_loading', true ) ) {
			new LazyLoading();
		}

		new CustomClass();
		new RewriteRules();
		new ThumbnailDemand();

		$this->preload = new Preload();

		$this->woocommerce_integration = new WooCommerce();
		$this->woocommerce_integration->hooks();

		add_action( 'after_setup_theme', [ $this, 'setup' ], 5 );
		add_action( 'widgets_init', [ $this, 'register_sidebars' ], 5 );

		add_action( 'admin_bar_menu', [ $this, 'template_details' ], 99 );

		add_action( 'wp_footer', [ $this, 'wp_footer' ], 0 );
		add_action( 'wp_enqueue_scripts', [ $this, 'register_scripts' ], 20 );
		add_action( 'admin_enqueue_scripts', [ $this, 'register_admin_scripts' ], 200 );

		add_action( 'admin_init', [ $this, 'editor_styles' ] );

		add_action( 'theme/register_scripts/before', [ $this, 'register_scripts_before' ], 10, 3 );
		add_action( 'theme/register_scripts/after', [ $this, 'register_scripts_after' ], 10, 3 );

		add_filter( 'theme/flexible_content/exclude_layouts', [ $this, 'exclude_layouts' ], 10, 3 );

		add_filter( 'theme/deps/style', [ $this, 'style_deps' ] );
		add_filter( 'theme/deps/script', [ $this, 'script_deps' ] );

		add_filter( 'theme/deps/style', [ $this, 'both_deps' ] );
		add_filter( 'theme/deps/script', [ $this, 'both_deps' ] );

		add_filter( 'theme/localize/script', [ $this, 'localize_script' ] );

		add_filter( 'script_loader_tag', [ $this, 'set_async_script' ], 10, 2 );

		add_action( 'widgets_init', [ $this, 'register_widgets' ] );

		// Contact Form 7.
		add_filter( 'wpcf7_load_js', '__return_false' );
		add_filter( 'wpcf7_load_css', '__return_false' );
		add_filter( 'do_shortcode_tag', [ $this, 'run_cf7_scripts' ], 10, 2 );

		add_filter( 'mime_types', [ $this, 'mime_types' ] );
		add_filter( 'wp_check_filetype_and_ext', [ $this, 'wp_check_filetype_and_ext' ], 10, 5 );
		add_filter( 'jpeg_quality', [ $this, 'jpeg_quality' ] );

		// Gutenberg.
		add_filter( 'use_block_editor_for_post_type', [ $this, 'remove_gutenberg_support' ], 10, 2 );

		// Yoast Seo.
		add_filter( 'wpseo_metabox_prio', [ $this, 'wpseo_metabox_prio' ] );

		// Excerpt.
		add_filter( 'get_the_excerpt', 'trim', 0 );

		// Disable CSS / JS for WPML.
		define( 'ICL_DONT_LOAD_NAVIGATION_CSS', true );
		define( 'ICL_DONT_LOAD_LANGUAGES_JS', true );
		define( 'ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true );

		// Disables the block editor from managing widgets in the Gutenberg plugin.
		add_filter( 'gutenberg_use_widgets_block_editor', [ $this, 'use_widget_block_editor' ] );
		add_filter( 'use_widgets_block_editor', [ $this, 'use_widget_block_editor' ] );

		$this->register_fonts();

		$this->preload->hooks();
	}

	protected function register_fonts(): void {
	}

	/**
	 * @param mixed  $tag    .
	 * @param string $handle .
	 *
	 * @return string
	 */
	public function set_async_script( $tag, string $handle ): string {
		if ( in_array( $handle, $this->async_scripts, true ) ) {
			$tag = str_replace( '<script', '<script async', $tag );
		}

		return (string) $tag;
	}

	/**
	 * @return bool
	 */
	public function use_widget_block_editor(): bool {
		return $this->use_widget_block_editor;
	}

	/**
	 * @param string[]       $exclude_fields .
	 * @param string[]       $layouts        .
	 * @param WP_Screen|null $screen         .
	 *
	 * @return string[]
	 */
	public function exclude_layouts( $exclude_fields, array $layouts, ?WP_Screen $screen ): array {
		if ( $screen instanceof WP_Screen ) {
			return $exclude_fields;
		}

		return $exclude_fields;
	}

	public function run_cf7_scripts( $output, $tag ) {
		if ( function_exists( 'wpcf7_enqueue_scripts' ) && in_array( $tag, [ 'contact-form-7', 'contact-form' ], true ) ) {
			wpcf7_enqueue_scripts();
		}

		return $output;
	}

	/**
	 * Change metabox priority.
	 */
	public function wpseo_metabox_prio(): string {
		return 'low';
	}

	/**
	 * Filters list of allowed mime types and file extensions.
	 *
	 * @param array $mime_types      Mime types keyed by the file extension regex corresponding to
	 *                               those types. 'swf' and 'exe' removed from full list. 'htm|html' also
	 *                               removed depending on '$user' capabilities.
	 *
	 * @return array
	 */
	public function mime_types( $mime_types ): array {
		$mime_types['svg'] = 'image/svg+xml';

		return $mime_types;
	}

	public function wp_check_filetype_and_ext( $wp_check_filetype_and_ext, $file, $filename, $mimes, $real_mime ) {
		if ( $real_mime === 'image/svg' ) {
			return [
				'ext'             => 'svg',
				'type'            => $real_mime,
				'proper_filename' => false,
			];
		}

		return $wp_check_filetype_and_ext;
	}

	/**
	 * Default JPG Quality.
	 *
	 * @return int
	 */
	public function jpeg_quality(): int {
		return 100;
	}

	/**
	 * Register admin scripts.
	 */
	public function register_admin_scripts(): void {
		// Urls.
		$urls = $this->get_urls();

		// Register dirs.
		$dirs = $this->get_dirs();

		/**
		 * Main styles.
		 */
		$filename   = 'admin.css';
		$style_full = $dirs['dist'] . $filename;

		if ( file_exists( $style_full ) ) {
			$filename = $this->get_asset_file_ver( $style_full );
			wp_enqueue_style( 'main', $urls['dist'] . $filename, [], filemtime( $style_full ) );
		}
	}

	/**
	 * Register Widgets.
	 */
	public function register_widgets(): void {
		register_widget( Logo::class );
		register_widget( LanguageSwitcher::class );
	}

	/**
	 * Deps for CS and JS.
	 *
	 * @param array $deps An array of registered script handles this script depends on. Default empty array.
	 *
	 * @return array
	 */
	public function both_deps( array $deps ): array {
		return $deps;
	}

	/**
	 * Deps for JS.
	 *
	 * @param array $deps An array of registered script handles this script depends on. Default empty array.
	 *
	 * @return array
	 */
	public function script_deps( array $deps ): array {
		return $deps;
	}

	/**
	 * Deps for css.
	 *
	 * @param array $deps An array of registered script handles this script depends on. Default empty array.
	 *
	 * @return array
	 */
	public function style_deps( array $deps ): array {
		return $deps;
	}

	/**
	 * Main script localize.
	 *
	 * @param array $vars Array vars for JS.
	 *
	 * @return array
	 */
	public function localize_script( array $vars ): array {
		return $vars;
	}

	/**
	 * Before main script and style.
	 *
	 * @param array $urls Array of theme urls.
	 * @param array $dirs Array of theme dirs.
	 */
	public function register_scripts_before( array $urls, array $dirs ): void {
	}

	/**
	 * After main script and style.
	 *
	 * @param array $urls Array of theme urls.
	 * @param array $dirs Array of theme dirs.
	 */
	public function register_scripts_after( array $urls, array $dirs ): void {
	}

	/**
	 * Settings
	 */
	public function setup(): void {
		load_theme_textdomain( 'theme', get_template_directory() . '/languages' );

		add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'title-tag' );

		add_theme_support( 'post-thumbnails' );

		add_theme_support( 'post-formats', array_keys( get_post_format_strings() ) );

		add_theme_support( 'custom-background' );

		add_theme_support( 'custom-header' );

		add_theme_support( 'widgets' );

		add_theme_support( 'custom-logo' );

		add_theme_support( 'customize-selective-refresh-widgets' );

		add_theme_support( 'admin-bar', [ 'callback' => '__return_false' ] );

		add_theme_support( 'html5', [ 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ] );

		add_theme_support( 'woocommerce' );

		add_theme_support( 'wc-product-gallery-slider' );

		add_theme_support( 'wc-product-gallery-lightbox' );

		add_theme_support( 'ic-theme-templates' );

		add_theme_support( 'ic-theme-sections' );

		$this->register_menus();
		$this->add_image_sizes();

		$this->woocommerce_integration();
	}

	/**
	 * Current template info
	 *
	 * @param WP_Admin_Bar $wp_admin_bar WP_Admin_Bar instance, passed by reference.
	 */
	public function template_details( WP_Admin_Bar $wp_admin_bar ): void {
		global $template;

		if ( is_admin() ) {
			return;
		}

		$_template   = wp_normalize_path( $template );
		$content_dir = wp_normalize_path( WP_CONTENT_DIR . '/' );
		$_template   = str_replace( $content_dir, '', $_template );

		$wp_admin_bar->add_node(
			[
				'id'    => 'current-template-file',
				'title' => $_template,
			]
		);
	}

	/**
	 * Register editor style.
	 */
	public function editor_styles(): void {
		$dirs = $this->get_dirs();

		// Register urls.
		$filename   = 'editor.css';
		$style_full = $dirs['dist'] . $filename;

		if ( file_exists( wp_normalize_path( $style_full ) ) ) {
			$urls     = $this->get_urls();
			$filename = $this->get_asset_file_ver( $style_full );

			add_editor_style( $urls['dist'] . $filename );
		}
	}

	/**
	 * Get urls.
	 *
	 * @return string[]
	 */
	public function get_urls(): array {
		return [
			'template' => trailingslashit( get_theme_file_uri() ),
			'assets'   => get_theme_file_uri( 'assets/' ),
			'dist'     => get_theme_file_uri( 'assets/dist/' ),
			'vendor'   => get_theme_file_uri( 'assets/vendor/' ),
		];
	}

	/**
	 * Get dirs.
	 *
	 * @return string[]
	 */
	public function get_dirs(): array {
		return [
			'template' => get_theme_file_path( '/' ),
			'assets'   => get_theme_file_path( 'assets/' ),
			'dist'     => get_theme_file_path( 'assets/dist/' ),
			'vendor'   => get_theme_file_path( 'assets/vendor/' ),
		];
	}

	/**
	 * Register script and styles.
	 */
	public function register_scripts(): void {
		// Gutenberg block disable.
		if ( empty( $this->post_types_supported_gutenberg ) || ! is_singular( $this->post_types_supported_gutenberg ) ) {
			wp_dequeue_style( 'wp-block-library' ); // WordPress core.
		}

		wp_dequeue_style( 'wp-block-library-theme' ); // WordPress core.
		wp_dequeue_style( 'wc-block-style' ); // WooCommerce.
		wp_dequeue_style( 'storefront-gutenberg-blocks' ); // Storefront theme.

		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', includes_url( '/js/jquery/jquery.min.js' ), [], '3.6.4', $this->jquery_in_footer );

		// Urls.
		$urls = $this->get_urls();

		// Register dirs.
		$dirs = $this->get_dirs();

		do_action( 'theme/register_scripts/before', $urls, $dirs );

		/**
		 * Main styles.
		 */
		$filename   = 'theme.css';
		$style_full = $dirs['dist'] . $filename;

		if ( file_exists( $style_full ) ) {
			$filename_ver = $this->get_asset_file_ver( $style_full );
			$ver          = filemtime( $style_full );

			$this->preload->add( add_query_arg( 'ver', $ver, $urls['dist'] . $filename ), 'style', 'text/css' );

			wp_enqueue_style( 'main', $urls['dist'] . $filename_ver, apply_filters( 'theme/deps/style', [] ), $ver );
		}

		/**
		 * Main scripts.
		 */
		$filename     = 'theme.js';
		$scripts_full = $dirs['dist'] . $filename;
		if ( file_exists( $scripts_full ) ) {
			$filename_ver = $this->get_asset_file_ver( $scripts_full );

			$vars = [
				'url'      => [
					'home'     => trailingslashit( home_url() ),
					'ajax'     => admin_url( 'admin-ajax.php' ),
					'template' => trailingslashit( get_template_directory_uri() ),
					'api'      => get_rest_url(),
				],
				'locale'   => get_user_locale(),
				'language' => apply_filters( 'wpml_current_language', null ),
				'l10n'     => [],
				'cookie'   => [
					'path'   => COOKIEPATH,
					'domain' => defined( 'COOKIE_DOMAIN' ) ? COOKIE_DOMAIN : '.' . str_replace( 'www.', '', wp_parse_url( home_url(), PHP_URL_HOST ) ),
					'secure' => is_ssl(),
				],
				'nonce'    => [
					'api' => wp_create_nonce( 'wp_rest' ),
				],
			];

			wp_register_script( 'main', $urls['dist'] . $filename_ver, apply_filters( 'theme/deps/script', [] ), filemtime( $scripts_full ), true );
			wp_localize_script( 'main', '__jsVars', apply_filters( 'theme/localize/script', $vars ) );
		}

		do_action( 'theme/register_scripts/after', $urls, $dirs );
	}

	/**
	 * Register custom menu.
	 */
	public function register_menus(): void {
		register_nav_menus( [ 'primary' => __( 'Primary Menu', 'theme' ) ] );
	}

	/**
	 * Register sidebar.
	 */
	public function register_sidebars(): void {
		register_sidebar(
			[
				'name'          => __( 'Header', 'theme' ),
				'id'            => 'header',
				'description'   => '',
				'before_widget' => '<div id="%1$s" class="widget col %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			]
		);
	}

	/**
	 * Register image sizes.
	 */
	public function add_image_sizes(): void {
	}

	public function wp_footer(): void {
		wp_enqueue_script( 'main' );
	}

	/**
	 * Load filename from scheme.
	 *
	 * @param string $path File to load.
	 *
	 * @return string
	 */
	private function get_asset_file_ver( string $path ): string {
		if ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) {
			return wp_basename( $path );
		}

		if ( ! defined( 'ALLOW_VERSIONING_ASSETS' ) || ! ALLOW_VERSIONING_ASSETS ) {
			return wp_basename( $path );
		}

		/** @var array $file */
		$file = pathinfo( $path );

		return sprintf( '%s-%s-%d.%s', $file['filename'], RewriteRules::PREFIX, filemtime( $path ), $file['extension'] );
	}

	/**
	 * @param bool   $is_enabled
	 * @param string $post_type
	 *
	 * @return bool
	 */
	public function remove_gutenberg_support( $is_enabled, string $post_type ): bool {
		$supported = array_merge( $this->post_types_supported_gutenberg, get_post_types_by_support( 'gutenberg' ) );

		return in_array( $post_type, $supported, true );
	}

	protected function woocommerce_integration(): void {
	}
}
