<?php

namespace IC\Theme\Integration\WooCommerce;

use WC_Frontend_Scripts;

class WooCommerce {

	/** @var bool */
	private bool $load_js = true;

	/** @var bool */
	private bool $load_css = false;

	/**
	 * .
	 */
	public function hooks(): void {
		add_action( 'wp_enqueue_scripts', [ $this, 'unregister_scripts' ], 10000 );

		add_filter( 'woocommerce_enqueue_styles', [ $this, 'modify_woo_styles' ] );
	}

	public function unregister_scripts(): void {
		if ( ! function_exists( 'is_woocommerce' ) ) {
			return;
		}

		if ( ! $this->load_css || ! $this->maybe_allow_woo_assets() ) {
			wp_dequeue_style( 'wc-blocks-style' );
		}

		if ( ! $this->load_js || ! $this->maybe_allow_woo_assets() ) {
			remove_action( 'wp_enqueue_scripts', [ WC_Frontend_Scripts::class, 'load_scripts' ] );
			remove_action( 'wp_print_scripts', [ WC_Frontend_Scripts::class, 'localize_printed_scripts' ], 5 );
			remove_action(
				'wp_print_footer_scripts',
				[
					WC_Frontend_Scripts::class,
					'localize_printed_scripts',
				],
				5
			);
		}
	}

	/**
	 * @param mixed $styles .
	 *
	 * @return string[]
	 */
	public function modify_woo_styles( $styles ): array {
		if ( ! $this->load_css || ! $this->maybe_allow_woo_assets() ) {
			return [];
		}

		return $styles;
	}

	/**
	 * @param bool $load .
	 *
	 * @return self
	 */
	public function set_js_status( bool $load ): self {
		$this->load_js = $load;

		return $this;
	}

	/**
	 * @param bool $load .
	 *
	 * @return self
	 */
	public function set_css_status( bool $load ): self {
		$this->load_css = $load;

		return $this;
	}

	/**
	 * @return bool
	 */
	private function maybe_allow_woo_assets(): bool {
		return is_checkout() || is_cart() || is_post_type_archive( 'product' ) || is_account_page() || is_product();
	}
}
