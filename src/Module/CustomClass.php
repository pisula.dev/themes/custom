<?php
/**
 * Add custom fields for widgets.
 *
 * @package Theme
 */

namespace IC\Theme\Module;

/**
 * Custom field in widget.
 */
class CustomClass {
	/**
	 * Custom_Class constructor.
	 */
	public function __construct() {
		// Filters.
		add_filter( 'dynamic_sidebar_params', [ $this, 'dynamic_sidebar_params' ] );
		add_filter( 'widget_update_callback', [ $this, 'widget_update_callback' ], 10, 4 );

		// Actions.
		add_action( 'in_widget_form', [ $this, 'in_widget_form' ], 10, 3 );
	}

	/**
	 * Fires at the end of the widget control form.
	 * Use this hook to add extra fields to the widget form. The hook
	 * is only fired if the value passed to the 'widget_form_callback'
	 * hook is not false.
	 * Note: If the widget has no form, the text echoed from the default
	 * form method can be hidden using CSS.
	 *
	 * @param \WP_Widget $widget The widget instance, passed by reference.
	 * @param null       $return Return null if new fields are added.
	 * @param array      $instance An array of the widget's settings.
	 */
	public function in_widget_form( $widget, $return, $instance ) {

		if ( ! isset( $instance['classes'] ) ) {
			$instance['classes'] = '';
		}

		echo '<p>';
		echo '<label for="widget-' . esc_attr( $widget->id_base . '-' . $widget->number ) . '-classes">' . esc_html__( 'Custom classes', 'theme' ) . ':</label>';
		echo '<input type="text" name="widget-' . esc_attr( $widget->id_base ) . '[' . esc_attr( $widget->number ) . '][classes]" id="widget-' . esc_attr( $widget->id_base ) . '-' . esc_attr( $widget->number ) . '-classes" class="widefat" value="' . esc_attr( $instance['classes'] ) . '"/>';
		echo '</p>';
	}

	/**
	 * Filter a widget's settings before saving.
	 * Returning false will effectively short-circuit the widget's ability
	 * to update settings.
	 *
	 * @param array      $instance The current widget instance's settings.
	 * @param array      $new_instance Array of new widget settings.
	 * @param array      $old_instance Array of old widget settings.
	 * @param \WP_Widget $widget The current widget instance.
	 *
	 * @return array
	 */
	public function widget_update_callback( $instance, $new_instance, $old_instance, $widget ) {
		$instance['classes'] = $new_instance['classes'];

		return $instance;
	}

	/**
	 * Filter the parameters passed to a widget's display callback.
	 *
	 * @param array $params Params of sidebar.
	 *
	 * @return array
	 */
	public function dynamic_sidebar_params( $params ) {
		global $wp_registered_widgets;

		$classes = [];

		$widget_id  = $params[0]['widget_id'];
		$widget_obj = $wp_registered_widgets[ $widget_id ];
		$widget_opt = get_option( $widget_obj['callback'][0]->option_name );
		$widget_num = $widget_obj['params'][0]['number'];

		preg_match( '/class="(.*?)"/', $params[0]['before_widget'], $matches );

		if ( isset( $matches[1] ) && ! empty( $matches[1] ) ) {
			$classes = explode( ' ', $matches[1] );
		}

		if ( isset( $widget_opt[ $widget_num ]['classes'] ) && ! empty( $widget_opt[ $widget_num ]['classes'] ) ) {
			$classes = array_merge( $classes, explode( ' ', $widget_opt[ $widget_num ]['classes'] ) );
		}

		$classes = apply_filters( 'theme/widgets/widget_class', array_filter( $classes ), $widget_id, $widget_opt[ $widget_num ] );

		$params[0]['before_widget'] = preg_replace(
			'/class="(.*?)"/',
			'class="' . trim( implode( ' ', $classes ) ) . '"',
			$params[0]['before_widget'], 1
		);

		return $params;
	}
}
