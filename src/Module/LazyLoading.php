<?php
/**
 * Add custom fields for widgets.
 *
 * @package Theme
 */

namespace IC\Theme\Module;

/**
 * .
 */
class LazyLoading {

	/**
	 * @var bool
	 */
	private bool $renderer = false;

	/**
	 * Custom_Class constructor.
	 */
	public function __construct() {
		add_action( 'wp_footer', [ $this, 'add_lazy_loading_code' ], 0 );

		add_filter( 'theme/lazy_loading/image_attributes', [ $this, 'get_background_image_attribute_filter' ], 10, 4 );
	}

	/**
	 * @param string $value
	 * @param int    $image_id
	 * @param string $size
	 * @param string $breakpoint_mode
	 *
	 * @return false|string
	 */
	public function get_background_image_attribute_filter( $value, $image_id, $size = 'full', $breakpoint_mode = 'block' ) {
		return $this->get_background_image_attribute( $image_id, $size, $breakpoint_mode );
	}

	public function add_lazy_loading_code(): void {
		if ( ! $this->renderer ) {
			return;
		}
		?>
		<script type="text/javascript">
			(function () {
				this.performanceLoadService = function (customOptions) {
					var msie = window.document.documentMode;
					if (msie <= 11) {
						var service = this,
							defaultOptions = {
								debug: false,
								selector: '[data-performance-load]',
								attribute: 'data-performance-load',
								lazyLoad: true,
								lazyLoadRunOffset: 100,
								lazyLoadAllAfterTimeout: false,
								breakpointsMode: 'block'
							}, itemsToLoad;
					} else {
						var service = this,
							defaultOptions = {
								debug: false,
								selector: '[data-performance-load]',
								attribute: 'data-performance-load',
								lazyLoad: true,
								lazyLoadRunOffset: 300,
								lazyLoadAllAfterTimeout: false,
								breakpointsMode: 'block'
							}, itemsToLoad;
					}
					/** Helpers */
					service.helpers = function () {
						return {
							defaultBreakpointsMode: 'window',
							allowedBreakpointsModes: ['window', 'block'],
							extendSettings: function (source, properties) {
								var property;
								for (property in properties) {
									if (properties.hasOwnProperty(property)) {
										source[property] = properties[property];
									}
								}
								return source;
							},
							getCurrentBreakpointImage: function (element, options) {
								var imageUrl = '',
									type = '',
									currentWidth = 0,
									width = 0,
									sizes = Object.keys(options.breakpoints),
									breakpointMode = typeof options.breakpoint_mode !== 'undefined' ? options.breakpoint_mode : service.settings.breakpointsMode;
								var getUrl = function (size) {
									var url = '', typeOfDevise = '';
									if (typeof options.breakpoints[size] === 'string') {
										url = options.breakpoints[size];
									} else {
										if (service.helpers().isRetina() && typeof options.breakpoints[size].retina !== 'undefined') {
											typeOfDevise = 'retina';
											url = options.breakpoints[size].retina;
										} else {
											typeOfDevise = 'default';
											url = options.breakpoints[size].default;
										}
									}
									return {'url': url, 'typeOfDevise': typeOfDevise};
								};
								for (var i = 0; i < sizes.length; i++) {
									width = sizes[i];
									switch (breakpointMode) {
										case 'window':
											currentWidth = window.innerWidth;
											break;
										case 'block':
											if (element.parentNode) {
												currentWidth = element.parentNode.offsetWidth;
											} else {
												currentWidth = element.offsetWidth;
											}
											break;
									}
									if (service.settings.debug) {
										var debugWr = document.getElementsByClassName('debug');
										if (debugWr.length > 0) {
											debugWr[0].innerHTML =
												'<b>Debug enable</b> <br/>' +
												'Platform: ' + navigator.platform + ' <br/>' +
												'User agent: ' + navigator.userAgent + ' <br/>' +
												'Window width: ' + window.innerWidth + ' <br/>' +
												'Window height: ' + window.innerHeight + ' <br/>' +
												'Is high density: ' + service.helpers().isHighDensity() + ' <br/>' +
												'Is retina: ' + service.helpers().isRetina() + ' <br/><hr/>';
										}
									}
									if (typeof sizes[i + 1] !== 'undefined' && currentWidth < parseInt(sizes[i + 1])) {
										var data = getUrl(sizes[i]);
										imageUrl = data.url;
										type = data.typeOfDevise;
										break;
									} else {
										var data = getUrl(sizes[sizes.length - 1]);
										imageUrl = data.url;
										type = data.typeOfDevise;
										width = sizes[sizes.length - 1];
									}
								}
								return {'url': imageUrl, 'width': width, 'type': type, 'currentWidth': currentWidth};
							},
							isRetina: function () {
								return ((window.matchMedia && (window.matchMedia('only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx), only screen and (min-resolution: 75.6dpcm)').matches || window.matchMedia('only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2/1), only screen and (min--moz-device-pixel-ratio: 2), only screen and (min-device-pixel-ratio: 2)').matches)) || (window.devicePixelRatio && window.devicePixelRatio >= 2)) && /(iPad|iPhone|iPod|Mac)/g.test(navigator.userAgent);
							}
						}
					};
					/** Extend settings */
					service.settings = service.helpers().extendSettings(defaultOptions, customOptions);
					/** Service init */
					service.init = function () {
						if (service.helpers().allowedBreakpointsModes.indexOf(service.settings.breakpointsMode) === -1) {
							service.settings.breakpointsMode = service.helpers().defaultBreakpointsMode;
						}
						itemsToLoad = document.querySelectorAll(service.settings.selector);
						service.runProcessing();
						window.onresize = function () {
							service.runProcessing();
						};
						if (service.settings.lazyLoad) {
							window.onscroll = function () {
								service.runProcessing();
							}
						}
						if (service.settings.lazyLoadAllAfterTimeout !== false) {
							setTimeout(function () {
								service.settings.lazyLoad = false;
								service.runProcessing();
							}, service.settings.lazyLoadAllAfterTimeout);
						}
					};
					/** Research items for dynamic content */
					service.researchItems = function () {
						itemsToLoad = document.querySelectorAll(service.settings.selector);
					};
					/** Run service processing */
					service.runProcessing = function () {
						if (itemsToLoad.length > 0) {
							for (var i = 0; i < itemsToLoad.length; i++) {
								var element = itemsToLoad[i];
								if (service.settings.lazyLoad) {
									if ((window.pageYOffset + window.innerHeight) >= element.getBoundingClientRect().top + window.scrollY - service.settings.lazyLoadRunOffset) {
										service.setImage(element);
										element.style.opacity = 1;
									}
								} else {
									service.setImage(element);
									element.style.opacity = 1;
								}
							}
						}
					};
					/** Set image */
					service.setImage = function (element) {
						var json = element.getAttribute('data-performance-load');

						if (json.trim().length === 0) {
							return;
						}

						var options = JSON.parse(json);
						/** Get current breakpoint */
						var info = service.helpers().getCurrentBreakpointImage(element, options);
						if (element.nodeName === 'IMG') {
							element.src = info.url;
						} else {
							element.style.backgroundImage = "url('" + info.url + "')";
						}
						element.setAttribute('data-debug', 'URL: ' + info.url + '|Breakpoint: ' + info.width + '|Type: ' + info.type + '|Current Width: ' + info.currentWidth);
					};
					service.init();
				};
			}());
			window.performanceLoad = new performanceLoadService();

		</script>
		<?php
	}

	/**
	 * @param int          $image_id
	 * @param array|string $sizes
	 * @param string       $breakpoint_mode
	 *
	 * @return false|string
	 */
	private function get_background_image_attribute( $image_id, $sizes = 'full', $breakpoint_mode = 'block' ) {
		if ( ! wp_attachment_is_image( $image_id ) ) {
			return '';
		}

		if ( is_string( $sizes ) ) {
			$sizes = wp_parse_list( $sizes );
		}

		$breakpoints = [];
		foreach ( $sizes as $breakpoint => $size ) {
			$breakpoints[ $breakpoint ] = $this->get_image_variants( (int) $image_id, (string) $size );
		}

		$this->renderer = true;

		return wp_json_encode(
			[
				'breakpoint_mode' => $breakpoint_mode,
				'breakpoints'     => $breakpoints,
			],
			JSON_UNESCAPED_UNICODE
		);
	}

	/**
	 * @param int    $image_id .
	 * @param string $size     .
	 *
	 * @return array
	 */
	private function get_image_variants( int $image_id, string $size ): array {
		$breakpoints = [ 'default' => wp_get_attachment_image_url( $image_id, $size ) ];

		preg_match( '/([\d]+)x([\d]+)?/', $size, $args );

		if ( ! $args ) {
			return $breakpoints;
		}

		$retina_status = (float) get_post_meta( $image_id, 'retina_ratio', 1 );

		if ( ! $retina_status ) {
			$retina_status = 2;
		}

		$width  = 0;
		$height = 0;

		if ( count( $args ) === 3 ) {
			$width  = (int) $args[1];
			$height = (int) $args[2];
		} elseif ( count( $args ) === 2 ) {
			$width = (int) $args[1];
		}

		$width  *= $retina_status;
		$height *= $retina_status;

		$size = absint( $width ) . 'x';

		if ( $height > 0 ) {
			$size .= absint( $height );
		}

		if ( has_image_size( $size ) ) {
			$breakpoints['retina'] = wp_get_attachment_image_url( $image_id, $size );
		}

		return $breakpoints;
	}
}
