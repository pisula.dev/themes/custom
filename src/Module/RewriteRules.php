<?php
/**
 * Rewrite rules for dynamic file names
 *
 * @package Theme
 */

namespace IC\Theme\Module;

use WP_Rewrite;

/**
 * Class Rewrite_Rules
 *
 * @package Theme\Module
 */
class RewriteRules {
	public const PREFIX = 'ver';

	/**
	 * Rewrite_Rules constructor.
	 */
	public function __construct() {
		add_action( 'after_switch_theme', [ $this, 'after_switch_theme' ], 10, 2 );
		add_action( 'generate_rewrite_rules', [ $this, 'generate_rewrite_rules' ] );
	}

	/**
	 * Fires after the rewrite rules are generated.
	 *
	 * @param WP_Rewrite $wp_rewrite Current WP_Rewrite instance (passed by reference).
	 */
	public function generate_rewrite_rules( $wp_rewrite ): void {
		$content_dir = wp_basename( WP_CONTENT_DIR );
		$theme_path  = str_replace( WP_CONTENT_DIR, '', get_theme_file_path() );

		$wp_rewrite->add_external_rule(
			'(.*)' . ltrim( str_replace( WP_CONTENT_URL, '', get_theme_file_uri() ), '/' ) . '/assets/(.*)(-' . self::PREFIX . '-[0-9]+)(\..*)',
			$content_dir . $theme_path . '/assets/$2$4'
		);
	}

	/**
	 * Fires on the first WP load after a theme switch if the old theme still exists.
	 * This action fires multiple times and the parameters differs
	 * according to the context, if the old theme exists or not.
	 * If the old theme is missing, the parameter will be the slug
	 * of the old theme.
	 */
	public function after_switch_theme(): void {
		add_action( 'admin_init', 'save_mod_rewrite_rules', 1000 );
	}
}
