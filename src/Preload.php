<?php

namespace IC\Theme;

/**
 * .
 */
class Preload {

	/**
	 * @var array .
	 */
	private array $preload_resources = [];

	/**
	 * @return void
	 */
	public function hooks(): void {
		add_filter( 'wp_preload_resources', [ $this, 'preload_resources' ] );
	}

	/**
	 * @param $preload_resources
	 *
	 * @return array
	 */
	public function preload_resources( $preload_resources ): array {
		$preload_resources = is_array( $preload_resources ) ? $preload_resources : [];

		return array_merge( $preload_resources, $this->preload_resources );
	}

	/**
	 * @param string $href .
	 * @param string $as   .
	 * @param string $type .
	 *
	 * @return $this
	 */
	public function add( string $href, string $as, string $type ): Preload {
		$this->preload_resources[] = [
			'href' => $href,
			'as'   => $as,
			'type' => $type,
		];

		return $this;
	}
}
