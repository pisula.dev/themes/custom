<?php

namespace IC\Theme\Widget;

use WP_Widget;

/**
 * .
 */
class LanguageSwitcher extends WP_Widget {

	/**
	 * .
	 */
	public function __construct() {
		parent::__construct(
			'ic_language_switcher',
			__( 'Language Switcher', 'theme' )
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 *
	 * @see WP_Widget::widget()
	 */
	public function widget( $args, $instance ): void {
		$languages = $this->get_wpml_languages();

		if ( count( $languages ) <= 1 ) {
			return;
		}

		echo wp_kses_post( $args['before_widget'] );

		include locate_template( 'partials/widget/language-switcher.php' );

		echo wp_kses_post( $args['after_widget'] );
	}

	/**
	 * Back-end widget form.
	 *
	 * @param array $instance Previously saved values from database.
	 *
	 * @see WP_Widget::form()
	 */
	public function form( $instance ) {
	}

	/**
	 * @return array
	 */
	private function get_wpml_languages(): array {
		$languages = apply_filters( 'wpml_active_languages', null, [] );

		if ( empty( $languages ) ) {
			return [];
		}

		return $languages;
	}
}
