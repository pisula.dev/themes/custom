<?php

namespace IC\Theme\Widget;

use WP_Widget;

/**
 * .
 */
class Logo extends WP_Widget {

	/**
	 * .
	 */
	public function __construct() {
		parent::__construct(
			'ic_logo',
			__( 'Website Logo', 'theme' )
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 *
	 * @see WP_Widget::widget()
	 */
	public function widget( $args, $instance ): void {
		echo wp_kses_post( $args['before_widget'] );

		include locate_template( 'partials/widget/logo.php' );

		echo wp_kses_post( $args['after_widget'] );
	}

	/**
	 * Back-end widget form.
	 *
	 * @param array $instance Previously saved values from database.
	 *
	 * @see WP_Widget::form()
	 */
	public function form( $instance ) {
	}
}
